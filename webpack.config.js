var path = require('path')
var BundleTracker = require('webpack-bundle-tracker')

module.exports = {
  context: __dirname,
  entry: './assets/js/index',
  resolve: {modules: ['node_modules'], extensions: ['.js']},
  plugins: [new BundleTracker({filename: './webpack-stats.json'})],

  output: {path: path.resolve('./assets/bundles/'),
           filename: '[name]-[hash].js'},

  module: {loaders: [{test: /\.js$/,
                      exclude: /node_modules/,
                      loader: 'babel-loader',
                      query: {presets: ['react']}}]}}
