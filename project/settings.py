import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '*5fm(rch8ntqv4bren#5yyfjo*(9v$s&g86v&mp_y=d30zmgwm'
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'assets')]
ROOT_URLCONF = 'project.urls'
STATIC_URL = '/assets/'
TIME_ZONE = 'UTC'
USE_I18N = False
USE_TZ = True
DEBUG = True

INSTALLED_APPS = ['django.contrib.contenttypes',
                  'django.contrib.staticfiles',
                  'webpack_loader',
                  'app']

TEMPLATES = [{'BACKEND': 'django.template.backends.django.DjangoTemplates',
              'APP_DIRS': True}]

DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3',
                         'NAME': os.path.join(BASE_DIR, 'db.sqlite3')}}

WEBPACK_LOADER = {'DEFAULT': {
                    'BUNDLE_DIR_NAME': 'bundles/',
                    'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json')}}
